package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.nio.Buffer;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String province = "bangkok";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Do not allow the user to change the orientation of the application
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String, String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[] {
				"name", "value" }, new int[] { R.id.tvName, R.id.tvValue });
		setListAdapter(adapter);
		
		File dataFile = getBaseContext().getFileStreamPath("province.dat");
		
		if(dataFile.exists()){
			try{
				BufferedReader in = new BufferedReader(new FileReader(dataFile));
				province = in.readLine();
				in.close();
			}catch (FileNotFoundException e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			} catch (IOException e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		
		// Check if the device has Internet connection
		ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			// Load data when there is a connection
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 5 * 60 * 1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/"+province+"" +
						".json");
			}
		} else {
			Toast t = Toast
					.makeText(
							this,
							"No Internet Connectivity on this device. Check your setting",
							Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		

		switch (item.getItemId()) {
		case R.id.action_refresh:
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// Load data when there is a connection
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 3 * 60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/"+province+".json");
					Toast t = Toast.makeText(this, "Refreshing",
							Toast.LENGTH_LONG);
					t.show();
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			// Get the selected record
			
			return true;
			

		}
		switch (item.getItemId()) {
		case R.id.action_bkk:
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// Load data when there is a connection
				long current = System.currentTimeMillis();
				province = "bangkok";
				if (true) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
					Toast t = Toast.makeText(this, "Refreshing",
							Toast.LENGTH_LONG);
					t.show();
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			// Get the selected record
			
			return true;
			

		}
		switch (item.getItemId()) {
		case R.id.action_ntb:
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// Load data when there is a connection
				long current = System.currentTimeMillis();
				province = "nonthaburi";
				if (true) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/nonthaburi.json");
					Toast t = Toast.makeText(this, "Refreshing",
							Toast.LENGTH_LONG);
					t.show();
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			// Get the selected record
			
			return true;
			

		}
		switch (item.getItemId()) {
		case R.id.action_ptt:
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// Load data when there is a connection
				long current = System.currentTimeMillis();
				province = "pathumthani";
				if (true) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/pathumthani.json");
					Toast t = Toast.makeText(this, "Refreshing",
							Toast.LENGTH_LONG);
					t.show();
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			// Get the selected record
		
			return true;
			

		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.onSaveInstanceState(null);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (outState != null) {
			super.onSaveInstanceState(outState);
		}
		try {
			FileOutputStream outfile = openFileOutput("province.dat", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			
			
				p.write(province);
				p.flush(); p.close();
				outfile.close();
			
			
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
	}

	

	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		// Executed in UI thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}
	
		

		// Executed in UI thread
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), result,
					Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			setTitle(province+" Weather");
		}

		// Executed in Background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				// Get the first parameter, set it as a URL
				URL url = new URL(params[0]);
				// Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection) url
						.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				// We want to download data from URL
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);
					// Add "description"
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);

					String pressure = jmain.getString("pressure");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					record.put("value", pressure + " hPa");
					list.add(record);

					String humidity = jmain.getString("humidity");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					record.put("value", humidity + " %");
					list.add(record);

					String temp_min1 = jmain.getString("temp_min");
					record = new HashMap<String, String>();
					record.put("name", "Min Temperature");
					double temp_min = jmain.getDouble("temp_min") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp_min));
					list.add(record);
					
					
					String temp_max1
					= jmain.getString("temp_max");
					record = new HashMap<String, String>();
					record.put("name", "Max Temperature");
					double temp_max = jmain.getDouble("temp_max") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp_max));
					list.add(record);

					String speed = json.getJSONObject("wind")
							.getString("speed");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					record.put("value", speed + " mps");
					list.add(record);

					String deg = json.getJSONObject("wind").getString("deg");
					record = new HashMap<String, String>();
					record.put("name", "Wind Degree");
					record.put("value", deg + "degree");
					list.add(record);

					String all = json.getJSONObject("clouds").getString("all");
					record = new HashMap<String, String>();
					record.put("name", "All");
					record.put("value", all);
					list.add(record);

					return "Finished Loading Weather Data";
				}

				else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}
